window.onload = function() {
	var nav = document.getElementsByTagName('nav')[0];

	if(window.pageYOffset) {
		nav.classList.add('nav-shrink');
	}

	window.onscroll = function() {
		if(window.pageYOffset > 50) {
			// console.log(window.pageYOffset)
			nav.classList.add('nav-shrink');
		} else {
			nav.classList.remove('nav-shrink');
		}
	}

	$('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
     }
  	});

	function mail() {
		var mailOptions = {
			name: $('#form-name').val(),
			email: $('#form-email').val(),
			subject: $('#form-subj').val(),
			message: $('#form-message').val()
		};
		$.post('/send_mail', mailOptions, function(data) {
			alert('Swag: ' + data)
			$('#form-name').val('');
			$('#form-email').val('');
			$('#form-subj').val('');
			$('#form-message').val('');
		})
	}

	$('#form-submit').click(mail);
}