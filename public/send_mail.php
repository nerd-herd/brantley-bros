<?php
    require("../includes/config.php");
    if (empty($_GET["options"]))
    {
        http_response_code(400);
        exit;
    }
    $res = json_decode($_GET["options"]);
    $message = "Name: '".$res->{"name"}."'\nEmail: '".$res->{"email"}."'\nSubject: '".$res->{"subject"}."'\nMessage: '".$res->{"message"}."'";
    if (sendEmail("New Contact Form Received From ".$res->{"name"}." (".$res->{"email"}.")", $message, "autocsfinance@gmail.com", "Brantley Bros. Auto-Responder")) {
        header("Content-type: application/json");
        print(json_encode("Mail Sent Successfully!", JSON_PRETTY_PRINT));
    } else {
        header("Content-type: application/json");
        print(json_encode("Mail Send Failure!", JSON_PRETTY_PRINT));
    }
?>