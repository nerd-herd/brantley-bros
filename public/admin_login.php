<?php
    require("../includes/config.php");
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        if (!empty($_SESSION["id"])) {
            redirect("admin_panel.php");
        } else {
            render("admin_login_form.php");
        }
    } else {
        $res = query("SELECT * FROM admins WHERE user = ?", $_POST["user"]);
        if (array_key_exists(0, $res)) {
            if (password_verify($_POST["pass"], $res[0]["hash"])) {
                $_SESSION["id"]=$_POST["user"];
                redirect("admin_panel.php");
            } else {
                render("admin_login_form.php", ["notify" => true, "notify_message" => "Invalid Password!", "notify_type" => "alert alert-danger"]);
            }
        } else {
            render("admin_login_form.php", ["notify" => true, "notify_message" => "User Not Found!", "notify_type" => "alert alert-danger"]);
        }
    }
?>