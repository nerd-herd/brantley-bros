<?php
    require("../includes/config.php");
    if (empty($_SESSION["id"])) {
        redirect("admin_login.php");
    } else {
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            $res = query("SELECT * FROM adminpanel");
            $res[0]["show"] = "";
            render("admin_update_form.php", $res[0]);
        } else {
            $success = true;
            for ($i = 1; $i <= 4; $i++) {
                $res = query("UPDATE adminpanel SET box".$i." = ?, header".$i." = ?", $_POST["box".$i], $_POST["header".$i]);
                if ($res === false) {
                    $success=false;
                }
            }
            for ($i = 1; $i <= 3; $i++) {
                $res = query("UPDATE adminpanel SET writer".$i." = ?, testimonial".$i." = ?", $_POST["writer".$i], $_POST["testimonial".$i]);
                if ($res === false) {
                    $success=false;
                }
            }
            if (!$success) {
                $show = "Update Failed!";
                $showtype = "alert alert-danger";
            } else {
                $show = "Update Successful!";
                $showtype = "alert alert-success";
            }
            $res = query("SELECT * FROM adminpanel");
            $res[0]["notify"] = true;
            $res[0]["notify_message"] = $show;
            $res[0]["notify_type"] = $showtype;
            render("admin_update_form.php", $res[0]);
        }
    }
?>