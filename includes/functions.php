<?php

    require('_lib/class.phpmailer.php');
    require('_lib/class.smtp.php');

    function query(/* $sql [, ... ] */)
    {
        // SQL statement
        $sql = func_get_arg(0);

        // parameters, if any
        $parameters = array_slice(func_get_args(), 1);

        // try to connect to database
        static $handle;
        if (!isset($handle))
        {
            try
            {
                // connect to database
                $handle = new PDO("mysql:dbname=" . DATABASE . ";host=" . SERVER, USERNAME, PASSWORD);

                // ensure that PDO::prepare returns false when passed invalid SQL
                $handle->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); 
            }
            catch (Exception $e)
            {
                // trigger (big, orange) error
                trigger_error($e->getMessage(), E_USER_ERROR);
                exit;
            }
        }

        // prepare SQL statement
        $statement = $handle->prepare($sql);
        if ($statement === false)
        {
            // trigger (big, orange) error
            trigger_error($handle->errorInfo()[2], E_USER_ERROR);
            exit;
        }

        // execute SQL statement
        $results = $statement->execute($parameters);

        // return result set's rows, if any
        if ($results !== false)
        {
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }
        else
        {
            return false;
        }
    }
    
    function redirect($destination) {
        // handle URL
        if (preg_match("/^https?:\/\//", $destination))
        {
            header("Location: " . $destination);
        }

        // handle absolute path
        else if (preg_match("/^\//", $destination))
        {
            $protocol = (isset($_SERVER["HTTPS"])) ? "https" : "http";
            $host = $_SERVER["HTTP_HOST"];
            header("Location: $protocol://$host$destination");
        }

        // handle relative path
        else
        {
            // adapted from http://www.php.net/header
            $protocol = (isset($_SERVER["HTTPS"])) ? "https" : "http";
            $host = $_SERVER["HTTP_HOST"];
            $path = rtrim(dirname($_SERVER["PHP_SELF"]), "/\\");
            header("Location: $protocol://$host$path/$destination");
        }

        // exit immediately since we're redirecting anyway
        exit;
    }
    
    function render($template, $values = []) {
        if (file_exists("../templates/$template"))
        {
            extract($values);
            if (in_array(basename($_SERVER["PHP_SELF"]), ["admin_panel.php", "admin_login.php"])) {
                $nav = "nav-shrink";
            } else {
                $nav = "nav";
            }
            require("../templates/head.php");
            if (empty($_SESSION["id"])) {
                require("../templates/nav.php");
            } else {
                require("../templates/nav_admin.php");
            }
            if (isset($notify)) {
                if ($notify) {
                    require("../templates/notify.php");
                }
            }
            require("../templates/$template");
        } else {
            trigger_error("Invalid template: $template", E_USER_ERROR);
        }
    }
    
    function sendEmail($subject,$body,$receiver, $name) {
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "ssl";
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465;
        $mail->Username = "autocsfinance@gmail.com";
        $mail->Password = 'c$finance';
        $mail->SetFrom('autocsfinance@gmail.com', 'CS Finance Auto-Responder');
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AddAddress($receiver, $name);
   
        if (!$mail->Send())
        {
            return false;
        } else {
            return true;
        }
    }
?>