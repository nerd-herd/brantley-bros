<html>
	<head lang="en">
		<meta charset="UTF-8">
		<title>Brantley Brothers Moving</title>
		<link rel="stylesheet" href="../includes/css/bp.css">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="../includes/css/main.css"> 
		<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
		<script src="../includes/js/main.js"></script>
	</head>

	<body>
		<a name="home">
		<nav>
			<div class="<?=$nav?>">
				<a style="float: left; font-size: 40px; margin: 15px 0 0 20px" href="#home">Brantley Brothers Moving</a>
				<a id="mobile-dropdown"><i class="fa fa-bars"></i></a>
				<ul>