

		<div class="main-content">
			<div class="container text-center" style="padding-top: 250px; color: white">
			<h1 style="font-size: 100px">Brantley Brothers</h1>
			<a class="btn btn-default" id="request-move-btn">REQUEST A MOVE</a>
			</div>
		</div>

		<div class="about">
			<a name="about" id="anchor-about"></a>
			<div class="container">
				<div class="about-content">
					<div class="about-header">
						<div class="icon"><img src="../includes/imgs/icon.png" style="height: 80px"><i class="fa fa-users"></i></div>
						<h1><?=$header1?></h1>
					</div>
					<p><?=$box1?></p>
					<hr>
				</div>

				<div class="about-content">
					<div class="about-header">
						<div class="icon"><img src="../includes/imgs/icon.png" style="height: 80px"><i class="fa fa-heart"></i></div>
						<h1><?=$header2?></h1>
					</div>
					<p><?=$box2?></p>
					<hr>
				</div>

				<div class="about-content">
					<div class="about-header">
						<div class="icon"><img src="../includes/imgs/icon.png" style="height: 80px"><i class="fa fa-hand-rock-o"></i></div>
						<h1><?=$header3?></h1>
					</div>
					<p><?=$box3?></p>
					<hr>
				</div>

				<div class="about-content">
					<div class="about-header">
						<div class="icon"><img src="../includes/imgs/icon.png" style="height: 80px"><i class="fa fa-truck"></i></div>
						<h1><?=$header4?></h1>
					</div>
					<p><?=$box4?></p>
				</div>
				<br>
			</div>
		</div>

		<div class="jumbotron">
			<a name="testimonials" id="anchor-test"></a>
			<div class="testimonials">
				<blockquote>
					<p><b><?=$writer1?></b></p>
					<p><?=$testimonial1?></p> 
					<!--<p>Warmly, Irene Cooper</p>-->
				</blockquote>
				<blockquote>
					<p><b><?=$writer2?></b></p>
					<p><?=$testimonial2?></p>
				</blockquote>
				<blockquote>
					<p><b><?=$writer3?></b></p>
					<p><?=$testimonial3?></p>
				</blockquote>

			</div>

		</div>

		<div class="contact">
			<a name="contact" id="anchor-contact"></a>
			<div class="container" style="margin-top: -20px">
				<h1>Contact Us</h1>
				<br>
				<form id="mail-contact">
					<div class="form-group">
						<label>Name</label>
						<input type="text" id="form-name" name="name" class="form-control" placeholder="John Snow" required>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="text" id="form-email" name="email" class="form-control" placeholder="johnsnow@example.com" required>
					</div>
					<div class="form-group">
						<label>Subject</label>
						<input type="text" id="form-subj" name="subject" class="form-control" placeholder="Subject Line" required>
					</div>
					<div class="form-group">
						<label>Message</label>
						<textarea id="form-message" name="message" class="form-control" rows="4" placeholder="Hi Brantley Bros..." required></textarea>
					</div>

					<button type="submit" id="form-submit" class="btn btn-warning">SUBMIT</button>
				</form>
				<br><br>
			</div>
		</div>

		<footer>
			<div class="footer">
				<div class="container">
					<h1>Brantley Bros © 2006-2015</h1>
				</div>
				<div style="width: 20%;margin: 0 auto;">
                    168 Elizabeth Avenue
                    <br>
                    Newark, NJ 07108
                    <br>
                    Ph: 973.824.9500
                    <br>
                    Fax: 973.242.5729
                    <br>
                    E-mail:
                    <a style="color: white" href="mailto:info@brantleybros.com">info@brantleybros.com</a>
                    <br>
				</div>
				<div id="footer-bottom"></div>
			</div>
		</footer>
	</body>
</html>